import './styles/style.css';
import {header} from "./modules/header";
import {main} from "./modules/main";
import { wrapDivs } from './modules/wrapDivs';
import {wrapPageContent} from './modules/pageContent';
import {inputSearch} from './modules/inputSearch';
import createSelect from './modules/select';
import createAddBtn from './modules/addBtn';

//==========root===================
const root = document.getElementById('root');
//=======header main page===========
root.append(header);
//=========content main page========
root.append(main);

//=========wrapDivs================
document.querySelector('.container').append(wrapDivs);

const outputTitlePage = () => {
    const title = document.querySelector('.title');
    const mainTitle = document.createElement('h1');
    mainTitle.className = 'main__title';
    mainTitle.textContent = `Найди любимого персонажа “Gravity Falls”`;
    title.append(mainTitle);
};
outputTitlePage();

const outputSubTitle = () => {
    const subTitle = document.querySelector('.subtitle');
    const paragraph = document.createElement('p');
    paragraph.className = 'main__subtitle';
    paragraph.textContent = 'Вы сможете узнать тип героев, их способности, сильные стороны и недостатки.';
    subTitle.append(paragraph);
};
outputSubTitle();

const addButton = () => {
    const start = document.querySelector('.start');
    const button = document.createElement('button');
    button.className = 'main__button';
    button.setAttribute('value', 'Начать');
    button.textContent = 'Начать';
    start.append(button);
};
addButton();

//=========pageContent==============
document.querySelector('.container').append(wrapPageContent);

//==========input search============
document.querySelector('.search__page-content').append(inputSearch);

//==========selects=================
document.querySelector('.wrapper-choice').append(createSelect('Пол', 'Мужской', 'Женский', 'Неопределен'));
document.querySelector('.wrapper-choice').append(createSelect('Раса', 'Человек', 'Монстр', 'Неизвестно'));
document.querySelector('.wrapper-choice').append(createSelect('Сторона', 'Порядок', 'Хаос', 'Неизвестно'));
document.querySelector('.wrapper-choice').append(createAddBtn({type: 'button', value: 'Добавить   +'}));

const btn = document.querySelector('.main__button');
btn.addEventListener('click', () => {
    main.className = 'page__content';
    wrapDivs.style.display = 'none';
    wrapPageContent.style.display = 'block';
    
});

const links = document.querySelectorAll('a');
links.forEach(item => {
    item.addEventListener('click', (event) => {
        event.preventDefault();
        if (event.target.matches('a[href="/Персонажи"]')) {
            main.className = 'page__content';
            wrapDivs.style.display = 'none';
            wrapPageContent.style.display = 'block';
        } else if (event.target.matches('a[href="/Главная"]')) {
            main.className = 'main';
            wrapDivs.style.display = 'block';
            wrapPageContent.style.display = 'none';
        }

    });
});

document.querySelector('.wrapper-choice').addEventListener('click', (event) => {
    console.log(event.target);
});
