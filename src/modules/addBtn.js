import {addAtributes} from '../index';


const createAddBtn = (obj) => {
    const addBtn = document.createElement('input');
    addBtn.className = 'input__add-btn';
    for (let key in obj) {
        addBtn.setAttribute(key, obj[key]);
    }
    return addBtn;
};

export default createAddBtn;