const addWrapDivs = () => {
    const wrapDivs = document.createElement('div');
    wrapDivs.className = 'main__wrap-divs';

    const divs = (...args) => {
        args.forEach(item => {
            const div = document.createElement('div');
            div.className = item;
            wrapDivs.append(div);
        });
    };

    divs('title', 'subtitle', 'start');
    
    return wrapDivs;
};
const wrapDivs = addWrapDivs();

export {wrapDivs};