const pageContent = (...args) => {
    const arr = args;
    const wrapPageContent = document.createElement('div');
    wrapPageContent.className = arr[0];

    const addForm = (...args) => {
        const wrapForm = document.createElement('div');
        const form = document.createElement('form');
        const arr = args;

        wrapForm.className = 'wrap__form';
        wrapPageContent.append(wrapForm);
        form.className = 'form__page-content';
        wrapForm.append(form);

        arr.forEach(item => {
            const div = document.createElement('div');
            div.className = item;
            form.append(div);
        });
        
    };
    addForm('search__page-content', 'wrapper-choice');
    
    return wrapPageContent;
};

const wrapPageContent = pageContent('wrap__page-content');

export {wrapPageContent};