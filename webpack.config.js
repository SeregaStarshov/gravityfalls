const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/index.js',
    mode: 'development',
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].bundle.js'
    },
    plugins: [
        new HTMLWebpackPlugin({
            template: path.resolve(__dirname, './src/template.html'),
            filename: 'index.html'
        }),
    ],
    module: {
		rules: [
			{
				test: /\.js$/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/env'],
					},
				},
				exclude: /node_modules/,
			},
            {
                //css scss
                test: /\.(scss|css)$/,
                use: ['style-loader', 'css-loader']
            },
            {
                //jpg
                test: /\.(png|jpg|jpeg|svg|gif)$/,
                type: 'asset/resource',
            },
            {
                //шрифты
                test: /\.(woff(2)?|eot|ttf|otf|svg|)$/,
                type: 'asset/inline'
            }
		]
	},
    devServer: {
        historyApiFallback: true,
        open: true,
        compress: true,
        hot: true,
        port: 8080,
    },
}